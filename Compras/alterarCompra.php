<?php
    require_once "../verifica.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <title>Compra - Alterar Compra</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-link text-white btn btn-outline-primary" href="compras.php">Voltar</a>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-white" href="../home.php">Página Inicial</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Departamentos
                    </a>
                    <div class="dropdown-menu bg-dark mudar-cor" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item text-white" href="compras.php">Compras</a>
                    <a class="dropdown-item text-white" href="../Fornecedor/fornecedores.php">Fornecedores</a>
                    <a class="dropdown-item text-white" href="../Produto/produtos.php">Produtos</a>
                    <a class="dropdown-item text-white" href="../Usuario/usuarios.php">Usuários</a>
                    <a class="dropdown-item text-white" href="../Vendas/vendas.php">Vendas</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white btn btn-danger" href="../sair.php">Sair</a>
                </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="container"><br>
        <div class="form-group modelo-divs bg-dark"><br>
            <h3 class="texto-centro text-white">Alterar compra</h3><br>
            <?php
                require_once 'classeCompra.php';
                $c = new compra();
                if(isset($_POST['data'])){
                    if(isset($_GET['id'])){
                        $c->setId($_GET['id']);
                        $c->setData($_POST['data']);
                        $c->setIdFornecedor($_POST['idFornecedor']);
                        $c->setNomeFornecedor($_POST['nomeFornecedor']);
                        $c->setCondicaoPagamento($_POST['condicaoPagamento']);
                        $c->setObservacaoPagamento($_POST['observacaoPagamento']);
                        $c->setDesconto($_POST['desconto']);
                        $c->setValorTotal($_POST['valorTotal']);
                        if ($c->alterar()){
                            echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                        <div class='modal-dialog' role='document'>
                                            <div class='modal-content'>
                                                <div class='modal-header bg-success'>
                                                    <h5 class='modal-title text-white' id='exampleModalLabel'>Alteração realizada</h5>
                                                </div>
                                                <div class='modal-body'>
                                                    <p style='color:black'>A alteração foi realizada com sucesso!</p>
                                                </div>
                                                <div class='modal-footer'>
                                                    <a href='compras.php'><button type='button' class='btn btn-success'>Ok</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";  
                        }else{
                            echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                        <div class='modal-dialog' role='document'>
                                            <div class='modal-content'>
                                                <div class='modal-header bg-danger'>
                                                    <h5 class='modal-title text-white' id='exampleModalLabel'>Alteração não realizada</h5>
                                                </div>
                                                <div class='modal-body'>
                                                    Houve um erro no processo de alteração, o item não pode ser alterado!
                                                </div>
                                                <div class='modal-footer'>
                                                    <a href='compras.php'><button type='button' class='btn btn-danger'>Fechar</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";
                        }
                    }
                }else{
                    if(isset($_GET['id'])){
                        $c->setId($_GET['id']);
                        $resp=$c->buscarId();
            ?>	
                <form method="POST">
                    <div class="form-group text-white">
                        <label for="inputdata">Data: </label>
                        <input type="date" name="data" class="form-control" id="inputdata" value="<?php echo $resp['data']?>" autofocus>
                    </div>
                    <div class="form-group text-white">
                        <label for="inputidFornecedor">ID Fornecedor: </label>
                        <input type="number" name="idFornecedor" class="form-control" id="inputidFornecedor" value="<?php echo $resp['idFornecedor']?>">
                    </div>
                    <div class="form-group text-white">
                        <label for="inputnomeFornecedor">Nome Fornecedor: </label>
                        <input type="text" name="nomeFornecedor" class="form-control" id="inputnomeFornecedor" value="<?php echo $resp['nomeFornecedor']?>">
                    </div>
                    <div class="form-group text-white">
                        <label for="inputcondicaoPagamento">Condição de Pagamento:</label>
                        <select class="form-control" id="inputcondicaoPagamento" name="condicaoPagamento" value="<?php echo $resp['condicaoPagamento']?>">
                            <option type="text" value="a vista">A Vista</option>
                            <option type="text" value="cartao">Cartão</option>
                        </select>
                    </div>
                    <div class="form-group text-white">
                        <label for="inputobservacaoPagamento">Obervação de Pagamento:</label>
                        <textarea class="form-control" name="observacaoPagamento" id="inputobservacaoPagamento" rows="2" value="<?php echo $resp['observacaoPagamento']?>"></textarea>
                    </div>
                    <div class="form-group text-white">
                        <label for="inputdesconto">Desconto:</label>
                        <input type="number" name="desconto" class="form-control" id="inputdesconto" value="<?php echo $resp['desconto']?>">
                    </div>
                    <div class="form-group text-white">
                        <label for="inputvalorTotal">Valor Total:</label>
                        <input type="number" name="valorTotal" class="form-control" id="inputvalorTotal" value="<?php echo $resp['valorTotal']?>">
                    </div>
                    <div class="form-group text-white"><br>
                        <button class="btn btn-success" type="submit">Inserir</button>
                        <button class="btn btn-danger float-right" type="button"><a class="btn-cancelar" href='compras.php'>Cancelar</a></button>
                    </div>
                </form>
            <?php
                    }
                }
            ?>
        </div>   
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $('#exampleModal').modal('show')
    </script>
</body>
</html>