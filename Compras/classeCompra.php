<?php
	require_once '../conexaoBD/conexao.php';
	class compra{
		private $id;
		private $data;
		private $idFornecedor;
		private $nomeFornecedor;
        private $condicaoPagamento;
		private $observacaoPagamento;
		private $desconto;
		private $valorTotal;
		
		public function getId(){
			return $this->id;
		}
		public function setId($Id){
			$this->id=$Id;
		}
		
		public function getData(){
			return $this->data;
		}
		public function setData($Data){
			$this->data=$Data;	
		}
		
		public function getIdFornecedor(){
			return $this->idFornecedor;
		}
		public function setIdFornecedor($IdFornecedor){
			$this->idFornecedor=$IdFornecedor;
		}
		
		public function getNomeFornecedor(){
			return $this->nomeFornecedor;
		}
		public function setNomeFornecedor($NomeFornecedor){
			$this->nomeFornecedor=$NomeFornecedor;
        }
        
        public function getCondicaoPagamento(){
			return $this->condicaoPagamento;
		}
		public function setCondicaoPagamento($CondicaoPagamento){
			$this->condicaoPagamento=$CondicaoPagamento;
        }
        
        public function getObservacaoPagamento(){
			return $this->observacaoPagamento;
		}
		public function setObservacaoPagamento($ObservacaoPagamento){
			$this->observacaoPagamento=$ObservacaoPagamento;
        }
        
        public function getDesconto(){
			return $this->desconto;
		}
		public function setDesconto($Desconto){
			$this->desconto=$Desconto;
		}

		public function getValorTotal(){
			return $this->valorTotal;
		}
		public function setValorTotal($ValorTotal){
			$this->valorTotal=$ValorTotal;
		}
		
		public function buscarTodos(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from compras order by id"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function inserir(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"insert into compras(data,idFornecedor,nomeFornecedor,condicaoPagamento,observacaoPagamento,desconto,valorTotal)
					 values(:d, :idforn, :nforn, :cp, :obsp, :desc, :vt)"
				);
				$stmt->bindValue(":d", $this->getData());
				$stmt->bindValue(":idforn", $this->getIdFornecedor());
                $stmt->bindValue(":nforn", $this->getNomeFornecedor());
                $stmt->bindValue(":cp", $this->getCondicaoPagamento());
				$stmt->bindValue(":obsp", $this->getObservacaoPagamento());
				$stmt->bindValue(":desc", $this->getDesconto());
				$stmt->bindValue(":vt", $this->getValorTotal());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function excluir(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"delete from compras where id=:i"
				);
				$stmt->bindValue(":i", $this->getId());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarId(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from compras where id=:i"
				);
				$stmt->bindValue(":i", $this->getId());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"update compras set data=:d, idFornecedor=:idforn, nomeFornecedor=:nforn, condicaoPagamento=:cp, 
					observacaoPagamento=:obsp, desconto=:desc, valorTotal=:vt". 
					" where id=:id"
					
				);
				$stmt->bindValue(":id", $this->getId());
				$stmt->bindValue(":d", $this->getData());
				$stmt->bindValue(":idforn", $this->getIdFornecedor());
                $stmt->bindValue(":nforn", $this->getNomeFornecedor());
                $stmt->bindValue(":cp", $this->getCondicaoPagamento());
				$stmt->bindValue(":obsp", $this->getObservacaoPagamento());
				$stmt->bindValue(":desc", $this->getDesconto());
				$stmt->bindValue(":vt", $this->getValorTotal());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>