<?php
    require_once "../verifica.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css?version=12">
    <title>Venda - Alterar Venda</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-link text-white btn btn-outline-primary" href="vendas.php">Voltar</a>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-white" href="../home.php">Página Inicial</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Departamentos
                    </a>
                    <div class="dropdown-menu bg-dark mudar-cor dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item text-white" href="../Compras/compras.php">Compras</a>
                        <a class="dropdown-item text-white" href="../Fornecedor/fornecedores.php">Fornecedores</a>
                        <a class="dropdown-item text-white" href="../Produto/produtos.php">Produtos</a>
                        <a class="dropdown-item text-white" href="../Usuario/usuarios.php">Usuários</a>
                        <a class="dropdown-item text-white" href="vendas.php">Vendas</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white btn btn-outline-danger" href="../sair.php">Sair</a>
                </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="container"><br>
        <div class="form-group modelo-divs bg-dark"><br>
            <h3 class="texto-centro text-white">Alterar venda</h3><br>
            <?php
                require_once 'classeVenda.php';
                $c = new venda();
                if(isset($_POST['nome'])){
                    if(isset($_GET['id'])){
                        $c->setId($_GET['id']);
                        $c->setIDProduto($_POST['idProduto']);
                        $c->setQuantidade($_POST['quantidade']);
                        $c->setValorUnitario($_POST['valorUnitario']);
                        $c->setValorTotal($_POST['valorTotal']);
                        if ($c->alterar()){
                            echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                        <div class='modal-dialog' role='document'>
                                            <div class='modal-content'>
                                                <div class='modal-header bg-success'>
                                                    <h5 class='modal-title text-white' id='exampleModalLabel'>Alteração realizada</h5>
                                                </div>
                                                <div class='modal-body'>
                                                    <p style='color:black'>A alteração foi realizada com sucesso!</p>
                                                </div>
                                                <div class='modal-footer'>
                                                    <a href='vendas.php'><button type='button' class='btn btn-success'>Ok</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";  
                        }else{
                            echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                        <div class='modal-dialog' role='document'>
                                            <div class='modal-content'>
                                                <div class='modal-header bg-danger'>
                                                    <h5 class='modal-title text-white' id='exampleModalLabel'>Alteração não realizada</h5>
                                                </div>
                                                <div class='modal-body'>
                                                    Houve um erro no processo de alteração, o item não pode ser alterado!
                                                </div>
                                                <div class='modal-footer'>
                                                    <a href='vendas.php'><button type='button' class='btn btn-danger'>Fechar</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";
                        }
                    }
                }else{
                    if(isset($_GET['id'])){
                        $c->setId($_GET['id']);
                        $resp=$c->buscarId();
            ?>	
                <form method="POST">
                    <div class="form-group text-white">
                        <label for="inputidProduto">ID Produto: </label>
                        <input type="number" name="idProduto" class="form-control" id="inputidProduto" value="<?php echo $resp['idProduto']?>" autofocus>
                    </div>
                    <div class="form-group text-white">
                        <label for="inputquantidade">Quantidade: </label>
                        <input type="number" name="quantidade" class="form-control" id="inputquantidade" value="<?php echo $resp['quantidade']?>">
                    </div>
                    <div class="form-group text-white">
                        <label for="inputvalorUnitario">Valor Unitario(R$): </label>
                        <input type="number" step="any" min="0" name="valorUnitario" class="form-control" id="inputvalorUnitario" value="<?php echo $resp['valorUnitario']?>">
                    </div>
                    
                    <div class="form-group text-white">
                        <label for="inputvalorTotal">Valor Total(R$): </label>
                        <input type="number" step="any" min="0" name="valorTotal" class="form-control" id="inputvalorTotal" value="<?php echo $resp['valorTotal']?>">
                    </div>
                    <div class="form-group text-white"><br>
                        <button class="btn btn-success" type="submit">Inserir</button>
                        <button class="btn btn-danger float-right" type="button"><a class="btn-cancelar" href='vendas.php'>Cancelar</a></button>
                    </div>
                </form>
            <?php
                    }
                }
            ?>
        </div>   
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $('#exampleModal').modal('show')
    </script>
</body>
</html>