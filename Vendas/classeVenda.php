<?php
    require_once '../conexaoBD/conexao.php';
    class venda{
        private $id;
        private $idProduto;
        private $quantidade;
        private $valorUnitario;
        private $valorTotal;

        public function getId(){
            return $this->id;
        }

        public function setId($Id){
            $this->id = $Id;
        }

        public function getIdProduto(){
            return $this->idProduto;
        }

        public function setIdProduto($IdProduto){
            $this->idProduto = $IdProduto;
        }

        public function getQuantidade(){
            return $this->quantidade;
        }

        public function setQuantidade($Quantidade){
            $this->quantidade = $Quantidade;
        }

        public function getValorUnitario(){
            return $this->valorUnitario;
        }

        public function setValorUnitario($ValorUnitario){
            $this->valorUnitario = $ValorUnitario;
        }

        public function getValorTotal(){
            return $this->valorTotal;
        }

        public function setValorTotal($ValorTotal){
            $this->valorTotal = $ValorTotal;
        }

        public function inserir(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare(
                    "insert into vendas(idProduto, quantidade, valorUnitario, ValorTotal) 
                     values(:idProd, :qntd, :valoruni, :valortot)"
                );
                $stmt->bindValue(":idProd", $this->getIdProduto());
                $stmt->bindValue(":qntd", $this->getQuantidade());
                $stmt->bindValue(":valoruni", $this->getValorUnitario());
                $stmt->bindValue(":valortot", $this->getValorTotal());
                return $stmt->execute();
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            }
        }

        public function buscarTodos(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare("select * from vendas order by id");
                $stmt->execute();
                $r = $stmt->fetchAll();
                return $r;
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            }
        }

        public function buscarId(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare("select * from vendas where id=:i");
                $stmt->bindValue(":i", $this->getId());
                $stmt->execute();
                $r = $stmt->fetch();
                return $r;
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            } 
        }

        public function alterar(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare(
                    "update vendas set idProduto=:idProd, quantidade=:qntd, valorUnitario=:valoruni, ValorTotal=:valortot".
                    " where id=:id"
                );
                $stmt->bindValue(":id", $this->getId());
                $stmt->bindValue(":idProd", $this->getIdProduto());
                $stmt->bindValue(":qntd", $this->getQuantidade());
                $stmt->bindValue(":valoruni", $this->getValorUnitario());
                $stmt->bindValue(":valortot", $this->getValorTotal());
                return $stmt->execute();                                           
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            }
        }

        public function excluir(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare("delete from vendas where id=:id");
                $stmt->bindValue(":id", $this->getId());
                $stmt->execute();
                $r = $stmt->fetch();
                return $r;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
    }
?>