<?php
	require_once '../conexaoBD/conexao.php';
	class fornecedor{
		private $id;
		private $nome;
		private $cnpj;
        private $inscricaoEstadual;
        private $telefone;
        private $email;
        private $endereco;
		
		public function getId(){
			return $this->id;
		}
		public function setId($Id){
			$this->id=$Id;
		}
		
		public function getNome(){
			return $this->nome;
		}
		public function setNome($Nome){
			$this->nome=$Nome;	
		}
		
		public function getCnpj(){
			return $this->cnpj;
		}
		public function setCnpj($Cnpj){
			$this->cnpj=$Cnpj;
		}
		
		public function getInscricaoEstadual(){
			return $this->inscricaoEstadual;
		}
		public function setInscricaoEstadual($InscricaoEstadual){
			$this->inscricaoEstadual=$InscricaoEstadual;
        }
        
        public function getTelefone(){
			return $this->telefone;
		}
		public function setTelefone($Telefone){
			$this->telefone=$Telefone;
        }
        
        public function getEmail(){
			return $this->email;
		}
		public function setEmail($Email){
			$this->email=$Email;
        }
        
        public function getEndereco(){
			return $this->endereco;
		}
		public function setEndereco($Endereco){
			$this->endereco=$Endereco;
		}
		
		public function buscarTodos(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from fornecedor order by id"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function inserir(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"insert into fornecedor(nome, cnpj, inscricaoEstadual, telefone, email, endereco) values(:n, :c, :ie, :t, :e, :end)"
				);
				$stmt->bindValue(":n", $this->getNome());
				$stmt->bindValue(":c", $this->getCnpj());
                $stmt->bindValue(":ie", $this->getInscricaoEstadual());
                $stmt->bindValue(":t", $this->getTelefone());
				$stmt->bindValue(":e", $this->getEmail());
				$stmt->bindValue(":end", $this->getEndereco());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function excluir(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"delete from fornecedor where id=:i"
				);
				$stmt->bindValue(":i", $this->getId());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarId(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from fornecedor where id=:i"
				);
				$stmt->bindValue(":i", $this->getId());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"update fornecedor set nome=:n, cnpj=:c, inscricaoEstadual=:ie, telefone=:t, email=:e, endereco=:end". 
					" where id=:id"
				);
				$stmt->bindValue(":id", $this->getId());
				$stmt->bindValue(":n", $this->getNome());
				$stmt->bindValue(":c", $this->getCnpj());
                $stmt->bindValue(":ie", $this->getInscricaoEstadual());
                $stmt->bindValue(":t", $this->getTelefone());
				$stmt->bindValue(":e", $this->getEmail());
				$stmt->bindValue(":end", $this->getEndereco());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>