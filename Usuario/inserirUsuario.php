<?php
    require_once "../verifica.php";
?>
<!DOCTYPE html>
<html lang="pt-br">		
<head>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css?version=12">
    <title>Usuario - Inserir Usuario</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-link text-white btn btn-outline-primary" href="usuarios.php">Voltar</a>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-white" href="../home.php">Página Inicial</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Departamentos
                    </a>
                    <div class="dropdown-menu bg-dark mudar-cor dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item text-white" href="../Compras/compras.php">Compras</a>
                        <a class="dropdown-item text-white" href="../Fornecedor/fornecedores.php">Fornecedores</a>
                        <a class="dropdown-item text-white" href="../Produto/produtos.php">Produtos</a>
                        <a class="dropdown-item text-white" href="usuarios.php">Usuários</a>
                        <a class="dropdown-item text-white" href="../Vendas/vendas.php">Vendas</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white btn btn-outline-danger" href="../sair.php">Sair</a>
                </li>
                </ul>
            </div>
        </nav>
    </header>
    <?php
        require_once 'classeUsuario.php';
        $c= new usuario();
        if(isset($_POST['nome'])){
            $c->setNome($_POST['nome']);
            $c->setEmail($_POST['email']);
            $c->setSenha($_POST['senha']);
            if ($c->inserir()){
                echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                            <div class='modal-dialog' role='document'>
                                <div class='modal-content'>
                                    <div class='modal-header bg-success'>
                                        <h5 class='modal-title text-white' id='exampleModalLabel'>Cadastro realizado</h5>
                                        <button type='button' class='close text-white' data-dismiss='modal' aria-label='Close'>
                                        <span aria-hidden='true'>&times;</span>
                                        </button>
                                    </div>
                                    <div class='modal-body'>
                                        O cadastro foi realizado com sucesso!
                                    </div>
                                    <div class='modal-footer'>
                                        <a href='inserirUsuario.php'><button type='button' class='btn btn-success'>Ok</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>";  
            }else{
                echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                            <div class='modal-dialog' role='document'>
                                <div class='modal-content'>
                                    <div class='modal-header bg-danger'>
                                        <h5 class='modal-title text-white' id='exampleModalLabel'>Cadastro não realizado</h5>
                                        <button type='button' class='close text-white' data-dismiss='modal' aria-label='Close'>
                                        <span aria-hidden='true'>&times;</span>
                                        </button>
                                    </div>
                                    <div class='modal-body'>
                                        Houve um erro ao cadastrar, o item não pode ser cadastrado!
                                    </div>
                                    <div class='modal-footer'>
                                        <a href='inserirUsuario.php'><button type='button' class='btn btn-danger'>Fechar</button></a>
                                    </div>
                                </div>
                            </div>
                        </div>";
            }
            unset($_POST['nome']);
        }
    ?>
    <div class="container"><br>
        <div class="form-group modelo-divs bg-dark"><br>
            <h3 class="texto-centro text-white">Inserir Usuario</h3><br>
            <form method="POST">
                <div class="form-group text-white">
                    <label for="inputNome">Nome:</label>
                    <input type="text" name="nome" class="form-control" id="inputNome" placeholder="Nome do usuário" required autofocus>
                </div>
                <div class="form-group text-white">
                    <label for="inputEmail">Email:</label>
                    <input type="text" name="email" class="form-control" id="inputEmail" placeholder="Email do usuário" required>
                </div>
                <div class="form-group text-white">
                    <label for="inputSenha">Senha:</label>
                    <input type="text" name="senha" class="form-control" id="inputSenha" placeholder="Senha do usuário" required>
                </div>
                <div class="form-group text-white"><br>
                    <button class="btn btn-success" type="submit">Inserir</button>
                    <button class="btn btn-danger float-right" type="button"><a class="btn-cancelar" href='usuarios.php'>Cancelar</a></button>
                </div>
            </form>
        </div>
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $('#exampleModal').modal('show')
    </script>
</body>
</html>	