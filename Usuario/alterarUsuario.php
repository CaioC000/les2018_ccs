<?php
    require_once "../verifica.php";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css">
    <title>Usuario - Alterar Usuario</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-link text-white btn btn-outline-primary" href="usuarios.php">Voltar</a>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-white" href="../home.php">Página Inicial</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Departamentos
                    </a>
                    <div class="dropdown-menu bg-dark mudar-cor dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item text-white" href="../Compras/compras.php">Compras</a>
                        <a class="dropdown-item text-white" href="../Fornecedor/fornecedores.php">Fornecedores</a>
                        <a class="dropdown-item text-white" href="../Produto/produtos.php">Produtos</a>
                        <a class="dropdown-item text-white" href="usuarios.php">Usuários</a>
                        <a class="dropdown-item text-white" href="../Vendas/vendas.php">Vendas</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white btn btn-outline-danger" href="../sair.php">Sair</a>
                </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="container"><br>
        <div class="form-group modelo-divs bg-dark"><br>
            <h3 class="texto-centro text-white">Alterar usuario</h3><br>
            <?php
                require_once 'classeUsuario.php';
                $c = new usuario();
                if(isset($_POST['nome'])){
                    if(isset($_GET['id'])){
                        $c->setId($_GET['id']);
                        $c->setNome($_POST['nome']);
                        $c->setEmail($_POST['email']);
                        $c->setSenha($_POST['senha']);
                        if ($c->alterar()){
                            echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                        <div class='modal-dialog' role='document'>
                                            <div class='modal-content'>
                                                <div class='modal-header bg-success'>
                                                    <h5 class='modal-title text-white' id='exampleModalLabel'>Alteração realizada</h5>
                                                </div>
                                                <div class='modal-body'>
                                                    <p style='color:black'>A alteração foi realizada com sucesso!</p>
                                                </div>
                                                <div class='modal-footer'>
                                                    <a href='usuarios.php'><button type='button' class='btn btn-success'>Ok</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";  
                        }else{
                            echo "<div class='modal fade' id='exampleModal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>
                                        <div class='modal-dialog' role='document'>
                                            <div class='modal-content'>
                                                <div class='modal-header bg-danger'>
                                                    <h5 class='modal-title text-white' id='exampleModalLabel'>Alteração não realizada</h5>
                                                </div>
                                                <div class='modal-body'>
                                                    Houve um erro no processo de alteração, o item não pode ser alterado!
                                                </div>
                                                <div class='modal-footer'>
                                                    <a href='usuarios.php'><button type='button' class='btn btn-danger'>Fechar</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>";
                        }
                    }
                }else{
                    if(isset($_GET['id'])){
                        $c->setId($_GET['id']);
                        $resp=$c->buscarId();
            ?>	
                <form method="POST">
                    <div class="form-group text-white">
                        <label for="inputNome">Nome: </label>
                        <input type="text" name="nome" class="form-control" id="inputNome" value="<?php echo $resp['nome']?>" autofocus>
                    </div>
                    <div class="form-group text-white">
                        <label for="inputemail">Email: </label>
                        <input type="text" name="email" class="form-control" id="inputemail" value="<?php echo $resp['email']?>">
                    </div>
                    <div class="form-group text-white">
                        <label for="inputsenha">Senha: </label>
                        <input type="text" name="senha" class="form-control" id="inputsenha" value="<?php echo $resp['senha']?>">
                    </div>
                    <div class="form-group text-white"><br>
                        <button class="btn btn-success" type="submit">Inserir</button>
                        <button class="btn btn-danger float-right" type="button"><a class="btn-cancelar" href='usuarios.php'>Cancelar</a></button>
                    </div>
                </form>
            <?php
                    }
                }
            ?>
        </div>   
    </div>
    
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script>
        $('#exampleModal').modal('show')
    </script>
</body>
</html>