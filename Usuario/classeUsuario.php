<?php
    require_once '../conexaoBD/conexao.php';
    class usuario{
        private $id;
        private $nome;
        private $email;
        private $senha;

        public function getId(){
            return $this->id;
        }

        public function setId($Id){
            $this->id = $Id;
        }

        public function getNome(){
            return $this->nome;
        }

        public function setNome($Nome){
            $this->nome = $Nome;
        }

        public function getEmail(){
            return $this->email;
        }

        public function setEmail($Email){
            $this->email = $Email;
        }

        public function getSenha(){
            return $this->senha;
        }

        public function setSenha($Senha){
            $this->senha = $Senha;
        }

        public function inserir(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare(
                    "insert into usuario(nome, email, senha) values(:n, :e, :s)"
                );
                $stmt->bindValue(":n", $this->getNome());
                $stmt->bindValue(":e", $this->getEmail());
                $stmt->bindValue(":s", $this->getSenha());
                return $stmt->execute();
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            }
        }

        public function buscarTodos(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare("select * from usuario order by nome");
                $stmt->execute();
                $r = $stmt->fetchAll();
                return $r;
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            }
        }

        public function buscarId(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare("select * from usuario where id=:i");
                $stmt->bindValue(":i", $this->getId());
                $stmt->execute();
                $r = $stmt->fetch();
                return $r;
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            } 
        }

        public function alterar(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare(
                    "update usuario set nome=:n, email=:e, senha=:s".
                    " where id=:id"
                );
                $stmt->bindValue(":id", $this->getId());
                $stmt->bindValue(":n", $this->getNome());
                $stmt->bindValue(":e", $this->getEmail());
                $stmt->bindValue(":s", $this->getSenha());
                return $stmt->execute();                                           
            } catch(PDOException $e) {
                echo "Erro: ".$e->getMessage();
            }
        }

        public function excluir(){
            $c = new conexao();
            try {
                $stmt = $c->conn->prepare("delete from usuario where id=:id");
                $stmt->bindValue(":id", $this->getId());
                $stmt->execute();
                $r = $stmt->fetch();
                return $r;
            } catch(PDOException $e) {
                echo $e->getMessage();
            }
        }
    }
?>