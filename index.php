<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/style.css?version=12">
    <title>Projeto Web</title>
</head>
<body>
    <div id="caixa">

        <div id="login-label">
            <h2>Login</h2>
        </div>

        <form action="autentica.php" method="post">
            <div class="input-div">
                <input type="text"  name="email" placeholder="Email">
            </div>
            <div class="input-div" id="input-senha">
                <input type="password"  name="senha" placeholder="Senha">
            </div>
            <div id="botao">
                <button type="submit" class="btn btn-success btn-block">Entrar</button>
            </div> 
        </form>
        
    </div>
</body>
</html>