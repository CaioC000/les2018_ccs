<?php
    session_start();
	if (!isset($_SESSION['email'])) {
		session_destroy();
		header("Location:index.php");
		exit;
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css?version=12">
        <title>??</title>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-expand-lg bg-dark">
                <a class="navbar-brand text-white" href="../home.php">Home</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                    <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Departamentos
                        </a>
                        <div class="dropdown-menu bg-dark mudar-cor" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item text-white" href="Compras/compras.php">Compras</a>
                        <a class="dropdown-item text-white" href="Fornecedor/fornecedores.php">Fornecedores</a>
                        <a class="dropdown-item text-white" href="Produto/produtos.php">Produtos</a>
                        <a class="dropdown-item text-white" href="Usuario/usuarios.php">Usuários</a>
                        <a class="dropdown-item text-white" href="Vendas/vendas.php">Vendas</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-white btn btn-danger" href="sair.php">Sair</a>
                    </li>
                    </ul>
                </div>
            </nav>
        </header> 
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3>Bem vindo(a) <?php  echo $_SESSION['email']?></h3>
                </div>
            </div>
            <div class="row">
                <div class="col-4">
                    <a href="Produto/produtos.php"><div class="bg-dark">Produtos</div></a>
                </div>
                <div class="col-4">
                    <a href="Fornecedor/fornecedores.php"><div class="bg-dark">Fornecedores</div></a>
                </div>
                <div class="col-4">
                    <a href="Usuario/usuarios.php"><div class="bg-dark">Usuários</div></a>
                </div>
            </div> 
            <div class="row">
                <div class="col-4">
                    <a href="Compras/compras.php"><div class="bg-dark">Compras</div></a>
                </div>
                <div class="col-4">
                    <a href="Vendas/vendas.php"><div class="bg-dark">Vendas</div></a>
                </div>
            </div>   
        </div>
     
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </body>
</html>