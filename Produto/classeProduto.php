<?php
	require_once '../conexaoBD/conexao.php';
	class produto{
		private $id;
		private $nome;
		private $descricao;
		private $preco;
		
		public function getId(){
			return $this->id;
		}
		public function setId($Id){
			$this->id=$Id;
		}
		
		public function getNome(){
			return $this->nome;
		}
		public function setNome($Nome){
			$this->nome=$Nome;	
		}
		
		public function getDescricao(){
			return $this->descricao;
		}
		public function setDescricao($Descricao){
			$this->descricao=$Descricao;
		}
		
		public function getPreco(){
			return $this->preco;
		}
		public function setPreco($Preco){
			$this->preco=$Preco;
		}
		
		public function buscarTodos(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from produto order by id"
				);
				$stmt->execute();
				$r=$stmt->fetchAll();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function inserir(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"insert into produto(nome, descricao, preco) values(:n, :d, :p)"
				);
				$stmt->bindValue(":n", $this->getNome());
				$stmt->bindValue(":d", $this->getDescricao());
				$stmt->bindValue(":p", $this->getPreco());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function excluir(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"delete from produto where id=:i"
				);
				$stmt->bindValue(":i", $this->getId());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function buscarId(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"select * from produto where id=:i"
				);
				$stmt->bindValue(":i", $this->getId());
				$stmt->execute();
				$r=$stmt->fetch();
				return $r;
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
		
		public function alterar(){
			$c = new conexao();
			try{
				$stmt=$c->conn->prepare(
					"update produto set nome=:n, descricao=:d, preco=:p".
					" where id=:id"
				);
				$stmt->bindValue(":id", $this->getId());
				$stmt->bindValue(":n", $this->getNome());
				$stmt->bindValue(":d", $this->getDescricao());
				$stmt->bindValue(":p", $this->getPreco());
				return $stmt->execute();
			}catch(PDOException $e){
				echo $e->getMessage();
			}
		}
	}
?>