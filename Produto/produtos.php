<?php
    require_once "../verifica.php";
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Pacifico|Roboto+Slab:400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/style.css?version=12">
    <title>Produtos</title>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-dark">
            <a class="navbar-brand text-white" href="../home.php">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
                <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link text-white btn btn-primary" href="inserirProdutos.php">Incluir Produto</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link text-white dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Departamentos
                    </a>
                    <div class="dropdown-menu bg-dark mudar-cor dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item text-white" href="../Compras/compras.php">Compras</a>
                        <a class="dropdown-item text-white" href="../Fornecedor/fornecedores.php">Fornecedores</a>
                        <a class="dropdown-item text-white" href="produtos.php">Produtos</a>
                        <a class="dropdown-item text-white" href="../Usuario/usuarios.php">Usuários</a>
                        <a class="dropdown-item text-white" href="../Vendas/vendas.php">Vendas</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link text-white btn btn-danger" href="../sair.php">Sair</a>
                </li>
                </ul>
            </div>
        </nav>
    </header>
    <div class="container-fluid"><br>
        <div class="modelo-divs bg-dark"><br>
            <h3 class="texto-centro text-white">Produtos</h3><br>
            <?php
                require_once 'classeProduto.php';
                $c = new produto();
                $dados = $c->buscarTodos();
                echo "<table class='table table-hover table-dark table-bordered'>";
                echo "<thead>
                        <tr>
                            <th scope='col'>ID</th>
                            <th scope='col'>Nome</th>
                            <th scope='col'>Descrição</th>
                            <th scope='col'>Preço(R$)</th>
                            <th scope='col' colspan='2' class='texto-centro'>Opções</th>
                        </tr>
                    </thead>";
                foreach($dados as $linha){
                    print "<tr>";
                    print "<td scope='row'>".$linha['id']."</td>";
                    print "<td scope='row'>".$linha['nome']."</td>";
                    print "<td scope='row'>".$linha['descricao']."</td>";
                    print "<td scope='row'>".$linha['preco']."</td>";
                    print "<td scope='row' class='texto-centro'><a href='alterarProdutos.php?id=".$linha['id']."'><i class='material-icons' style='color: grey;'>create</i></a></td>";
                    print "<td scope='row' class='texto-centro'><a href='excluirProdutos.php?id=".$linha['id']."' data-confirm='Tem certeza que deseja excluir este item?'><i class='material-icons' style='color: red;'>remove_circle_outline</i></a></td>";
                    print "</tr>";
                }
                echo "</table>";
            ?>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    <script src="../js/confirmar-exclusao.js"></script>
</body>
</html>